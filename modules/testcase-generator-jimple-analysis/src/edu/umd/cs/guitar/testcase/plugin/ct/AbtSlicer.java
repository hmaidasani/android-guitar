/*	
 *  Copyright (c) 2011. The GREYBOX group at the University of Freiburg, Chair of Software Engineering.
 *  Names of owners of this group may be obtained by sending an e-mail to arlt@informatik.uni-freiburg.de
 * 
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 *  documentation files (the "Software"), to deal in the Software without restriction, including without 
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *	the Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 *	conditions:
 * 
 *	The above copyright notice and this permission notice shall be included in all copies or substantial 
 *	portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 *	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
 *	EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *	IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 *	THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package edu.umd.cs.guitar.testcase.plugin.ct;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import edu.umd.cs.guitar.testcase.plugin.ct.entity.event.CTEvent;

/**
 * @author arlt
 */
public class AbtSlicer {

	/**
	 * Soot Runner
	 */
	private CTSootRunner sootRunner;

	/**
	 * Body Transformer
	 */
	private CTBodyTransformer bodyTransformer;

	/**
	 * Slicer
	 */
	private CTSlicer slicer;

	/**
	 * C-tor
	 */
	public AbtSlicer() {

	}

	/**
	 * Transforms a Java program into a Jimple representation
	 * 
	 * @param scope
	 *            Scope (usually a JAR file or a directory)
	 * @param classpath
	 *            Classpath
	 * @param pakkage
	 *            Package to be analyzed
	 * @return true = success
	 */
	public boolean transform(String scope, String classpath, String pakkage) {
		// run Soot and get body transformer
		sootRunner = new CTSootRunner();
		bodyTransformer = sootRunner.run(scope, classpath, pakkage);
		return (null != bodyTransformer);
	}

	/**
	 * Slices the Jimple representation of the Java program
	 * 
	 * @param pastEvents
	 *            past events
	 * @param futureEvents
	 *            future events
	 * @return relevant future events
	 */
	public List<CTEvent> slice(List<CTEvent> pastEvents,
			List<CTEvent> futureEvents) {
		// run slicer
		List<CTEvent> events = new LinkedList<CTEvent>();
		events.addAll(pastEvents);
		events.addAll(futureEvents);
		slicer = new CTSlicer(bodyTransformer, events);
		slicer.run();

		// determine depending events
		List<CTEvent> relevantFutureEvents = new LinkedList<CTEvent>();
		CTEvent lastEvent = pastEvents.get(pastEvents.size() - 1);
		for (CTEvent event : futureEvents) {
			// determine common fields
			Set<String> fields = slicer.getCommonFields(lastEvent, event);
			if (!fields.isEmpty()) {
				relevantFutureEvents.add(event);
			}
		}
		return relevantFutureEvents;
	}

	/**
	 * Returns the current soot runner
	 * 
	 * @return Soot runner
	 */
	public CTSootRunner getCurrentSootRunner() {
		return sootRunner;
	}

	/**
	 * Returns the current body transformer
	 * 
	 * @return Body transformer
	 */
	public CTBodyTransformer getCurrentBodyTransformer() {
		return bodyTransformer;
	}

	/**
	 * Returns the current slicer
	 * 
	 * @return Slicer
	 */
	public CTSlicer getCurrentSlicer() {
		return slicer;
	}

}
