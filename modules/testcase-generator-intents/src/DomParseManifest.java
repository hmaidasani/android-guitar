/**
 * 
 */
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author Ely
 * 
 * 
 *         Reference:
 *         http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 */
public class DomParseManifest {

	public static void main(String argv[]) {

		try {
			// save the console output to a text file
			PrintStream sytmOut = System.out;
			PrintStream out = new PrintStream(new FileOutputStream(
					"parsedManifest.txt"));
			System.setOut(out);
			
			// start parsing the xml file
			File fXmlFile = new File(argv[0]);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			NodeList mList = doc.getElementsByTagName("manifest");
			//System.out.println(mList);
			/*
			 * ??????????????????? ask ely ??
			 * System.out.println("jean----------package: " +
			 * mList.item(0).getAttributes().getNamedItem("package"));
			 */
			// get package's information
			System.out.println("package: "
					+ mList.item(0).getAttributes().getNamedItem("package")
							.getNodeValue());
			// go to uses-permission tag
			NodeList pList = doc.getElementsByTagName("uses-permission");

			for (int temp = 0; temp < pList.getLength(); temp++) {
				Node nNode = pList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					System.out.println("permission: "
							+ eElement.getAttribute("android:name"));
				}
			}

			NodeList nList = doc.getElementsByTagName("activity");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				String isMain = "";
				Node nNode = nList.item(temp);

				// if( nNode.hasChildNodes()){
				// System.out.println(nNode.getNodeName());
				// Node a = nNode.getFirstChild();
				//
				// System.out.println(a.getNodeName());
				//
				//
				// }
				//
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					NodeList action = eElement.getElementsByTagName("action");
					if (action.getLength() > 0) {
						
						if (action.item(0).getAttributes()
								.getNamedItem("android:name").getNodeValue()
								.equals("android.intent.action.MAIN")) {

							NodeList category = eElement
									.getElementsByTagName("category");
							// //category can't be
							// 0?????????????????????????????????????
							if (category.getLength() > 0) {
								if (category
										.item(0)
										.getAttributes()
										.getNamedItem("android:name")
										.getNodeValue()
										.equals("android.intent.category.LAUNCHER")) {
									isMain = " (main launcher activity)";
								}

							}
						}
					}

					String  a =eElement.getAttribute("android:name") + isMain;
					if (!a.startsWith(".")){
						a= "."+a;
					}
					System.out.println("activity: "
							+ a);

				}
			}
			
			System.setOut(sytmOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
