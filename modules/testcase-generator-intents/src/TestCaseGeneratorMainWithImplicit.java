import java.io.BufferedReader;	
import java.io.BufferedWriter;	
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.Gson;


public class TestCaseGeneratorMainWithImplicit {

	static HashMap<String, List<Intent>> intentsMap = null;
	//static HashSet<HashMap<Integer, String>> completePath = new HashSet<HashMap<Integer, String>>();
	static ArrayList<String> completePath = new ArrayList<String>();
	static ArrayList<String> completeSequence = new ArrayList<String>();
	static String start;
	
	
	static String lastPath ="";
	static private final int MAX_SEQUENCE_LENGTH= 3;
	public static void main(String[] args) {
		/* get info from the json file */
		//DataObject obj = getJsonInfo("../../../IntentOutputs/out.txt");	//convert the json text into a DataObject
		DataObject obj = getJsonInfo(args[0]);	//convert the json text into a DataObject

		try {
			//Intent.parse("../../../IntentOutputs/parsedManifest.txt");
			Intent.parse(args[1]);
			//String start = Intent.getMainActivity("../../../IntentOutputs/parsedManifest.txt");
			String start = Intent.getMainActivity(args[1]);   /// pacakename/main activity

			intentsMap = Intent.getIntentsMap(obj);	//get the hashmap from each activity to list of Intents that are fired from that activity

			/* Find all possible sequence of intents using the hashmap */

			/* Generate the shell commands corresponding to each test case (sequence of intents) 
			 * print to a text file, .sh file, etc... */


			/** Siti changing from here -- trying to get the possible paths **/

			if(start == null){
				System.out.println("can't find main activity");
			} else {
				String path = new String(start);
				String sequence = new String("am start -n " + start);
				//iterate(path, start, sequence);
				iterate1(path, start, sequence);
				//iterateLoop(path, start, sequence,5);
				
				ArrayList<String> completePathTemp = new ArrayList<String>(completePath);
				
				ArrayList<String> temp = new ArrayList<String>();
				completePath.clear();
				
				for (int i = 0 ; i < completePathTemp.size() ;i++){
					boolean isRepeated = false;
					String path1 = completePathTemp.get(i);
					
					for (int j = completePathTemp.size()-1 ; j > i ; j--){
						String path2 = completePathTemp.get(j);
						
						if (path2.contains(path1)){
							isRepeated = true;
							//
							break;
						}
							
					}
					if (! isRepeated){
						temp.add(completePathTemp.get(i));
					}
				}
				completePath.addAll(temp);
				temp.clear();
				
				for (int i = 0 ; i < completeSequence.size() ;i++){
					boolean isRepeated = false;
					String path1 = completeSequence.get(i);
					
					for (int j = completeSequence.size()-1 ; j > i ; j--){
						String path2 = completeSequence.get(j);
						
						if (path2.contains(path1)){
							isRepeated = true;
							//
							break;
						}
							
					}
					if (! isRepeated){
						temp.add(completeSequence.get(i));
					}
				}
				
				completeSequence.clear();
				completeSequence.addAll(temp);
				temp.clear();
				
				for (Entry<String, List<Intent>> cur : intentsMap.entrySet()){
					System.out.println(cur.getKey()+ ": ");
					for (Intent I : cur.getValue()){
						System.out.println("\t" + I.action);
					}
				}
			}

			/*for(String src: intentsMap.keySet()){
					if(src == null)
						continue;
					String path = new String(src);
					iterate(path, src);
				}*/

			try {
				//FileWriter fstream2 = new FileWriter("../../../IntentOutputs/sequence.txt",true);
				//FileWriter fstream2 = new FileWriter(args[2],true);
				FileWriter fstream2 = new FileWriter(args[2],false);

				BufferedWriter out2 = new BufferedWriter(fstream2);
				for(String s: completePath){
					out2.append("\n" + s);
					System.out.println(s);
				}
				int i = 1;
				for(String s: completeSequence){
					out2.append("\n\nSequence " + i + "\n" + s);
					System.out.println("\n\n");
					System.out.println("Sequence " + i++);
					System.out.println(s);
				}
				out2.close();
			} catch (Exception e){//Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}
			/*for(String src: intentsMap.keySet()){
						if(intentsMap.get(src) == null)
							continue;
						System.out.println("src: " + src);
						for(Intent i: intentsMap.get(src)){
							String cmd = i.intentToCmd();
							System.out.println(cmd);
						}
						System.out.println();
					}*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/** Siti edit end **/
	}

	/* convert json formatted text files to DataObjects */
	public static DataObject getJsonInfo(String filename) {
		DataObject obj = null;
		Gson gson = new Gson();
		try {

			BufferedReader br = new BufferedReader(new FileReader(filename)); // filename of text file with json

			//convert the json string to object
			obj = gson.fromJson(br, DataObject.class);
			//System.out.println(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
	}


	/*this method does depth first search to map between activity
	 *  that launches the intent to the intent's activity*/
	public static void iterate(String pathArg, String curAct, String sequenceArg){
		String path = new String(pathArg);
		String sequence = new String(sequenceArg);
		System.out.println("curAct: " + curAct);
		if((!intentsMap.containsKey(curAct)) || (intentsMap.get(curAct) == null) || (intentsMap.get(curAct)).isEmpty()){
			if(!completePath.contains(path))
				completePath.add(new String(path));
			if(!completeSequence.contains(sequence))
				completeSequence.add(new String(sequence));
			return;
		}
		for(Intent targetIntent: intentsMap.get(curAct)){
			String act = targetIntent.activity;
			if((act == null) || path.contains(act)){
				if(!completePath.contains(path))
					completePath.add(new String(path));
				if(!completeSequence.contains(sequence))
					completeSequence.add(new String(sequence));
			} else {
				iterate(new String(path + " --> " + act), act, new String(sequence + "\n" + targetIntent.intentToCmd()));
			}
		}

	}

	/*this method does depth first search to map between activity
	 *  that launches the intent to the intent's activity*/
	public static void iterate1(String pathArg, String curAct, String sequenceArg){
		String path = new String(pathArg);
		String sequence = new String(sequenceArg);
		System.out.println("curAct: " + curAct);
		if((!intentsMap.containsKey(curAct)) || (intentsMap.get(curAct) == null) || (intentsMap.get(curAct)).isEmpty()){
			if(!completePath.contains(path))
				completePath.add(new String(path));
			if(!completeSequence.contains(sequence))
				completeSequence.add(new String(sequence));
			return;
		}
		for(Intent targetIntent: intentsMap.get(curAct)){
			String act = targetIntent.activity;
			System.out.println("\tAct: " + act);
			
			
			if(!completePath.contains(path))
					completePath.add(new String(path));
				if(!completeSequence.contains(sequence))
					completeSequence.add(new String(sequence));
			if((act == null ) ){
				
				if (targetIntent.action != null){ // if activity tag is null, this will display the intents action instead
					act =  "action:"+targetIntent.action;
					if (targetIntent.uri!= null){ // Not all implicit intents will have a uri
						act = act + " uri:"+targetIntent.uri;
					}
					//act = targetIntent.action + targetIntent.uri ;
					
					iterate1(new String(path + " --> " + act), act, new String(sequence + "\n" + targetIntent.intentToCmd()) );
				}
			}
			else {
				if (loopCount(path,act) < MAX_SEQUENCE_LENGTH){
				iterate1(new String(path + " --> " + act), act, new String(sequence + "\n" + targetIntent.intentToCmd()));
				}
			}
		}

	}

	
	private static int loopCount (String path, String act){
		int index = path.indexOf(act);
		int count = 0;
		while (index != -1) {
		    count++;
		    path = path.substring(index + 1);
		    index = path.indexOf(act);
		}
		return count;
	}


	public static void iterateLoop(String pathArg, String curAct, String sequenceArg,int SequenceLength){
		String path = new String(pathArg);
		String sequence = new String(sequenceArg);
		System.out.println("curAct: " + curAct);
		

		if((!intentsMap.containsKey(curAct)) || (intentsMap.get(curAct) == null) || (intentsMap.get(curAct)).isEmpty()){
			if(!completePath.contains(path))
				completePath.add(new String(path));
			if(!completeSequence.contains(sequence))
				completeSequence.add(new String(sequence));
			return;
		}
		if (SequenceLength> MAX_SEQUENCE_LENGTH){
			return;
		}
		for(Intent targetIntent: intentsMap.get(curAct)){
			String act = targetIntent.activity;
//			if (SequenceLength> MAX_SEQUENCE_LENGTH){
//				continue;
//			}
				if(!completePath.contains(path))
					completePath.add(new String(path));
				if(!completeSequence.contains(sequence))
					completeSequence.add(new String(sequence));

			/*
 			If the Intent is implicit it will not have a activity section in the intent call
			so we will display the action tag and the uri tag of the Intent 
			 */	
			if((act == null) ){

				

				if (targetIntent.action != null){ // if activity tag is null, this will display the intents action instead
					act =  "action:"+targetIntent.action;
					if (targetIntent.uri!= null){ // Not all implicit intents will have a uri
						act = act + " uri:"+targetIntent.uri;
					}
					//act = targetIntent.action + targetIntent.uri ;
					
				iterateLoop(new String(path + " --> " + act), act, new String(sequence + "\n" + targetIntent.intentToCmd()),SequenceLength );
				}

			} else {
//				if(!completePath.contains(path))
//					completePath.add(new String(path));
//				if(!completeSequence.contains(sequence))
//					completeSequence.add(new String(sequence));
				if ( path.contains(act)){
				SequenceLength++;
				}
				iterateLoop(new String(path + " --> " + act), act, new String(sequence + "\n" + targetIntent.intentToCmd()),SequenceLength );
			}
		}

	}

}

