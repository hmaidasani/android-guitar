import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



/**
 * This File is a simple parser which scans a GUI file and creates a mapping between
 * the text which appears on a wigit and the associated wigit ID.
 * Ex: A button with the text "click me" would get matched to the wigit id.
 * 
 * 
 * This file was intended to be used to expand the capability of 
 * Android Intents to include Implicit intents. the information of this file can be
 * combined with the output of the logcat during the ripping process to create a mapping between 
 * click events and the firing of an implicit intent. this mapping could then be used to create tst files
 * that incorparate implicit intents.
 * Logcat example output:
 * --------------------------------------------------------------------------------------------------------
D/edu.umd.cs.guitar(  375): getRootWindows: com.example.implicitintents2.CallIntentsActivity@43ea4620
D/edu.umd.cs.guitar(  375): socket close
D/edu.umd.cs.guitar(  375): click: Call Someone
I/ActivityManager(   85): Starting activity: Intent { act=android.intent.action.CALL dat=tel:(+49)12345789 cmp=com.android.phone/.OutgoingCallBroadcaster }
 * --------------------------------------------------------------------------------------------------------
 * 
 * As you can see from the above example activity "com.example.implicitintents2.CallIntentsActivity" connects to the "com.android.phone/.OutgoingCallBroadcaster"
 * activity through an implicit intent by clicking the button with the text "Call Someone". This file will generate a mapping that you can use to get the wigit id
 * of the button with the text "Call Someone".
 * 
 * 
 * An alternative( perhaps better) method of incorporating implicit intents would be to modify the ADR server   
 * @author Elysham@terpmail.umd.edu
 *
 */
public class ParseWigitIdFromGui {
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File fXmlFile = new File("Demo.GUI");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();



			//NodeList guiList = doc.getElementsByTagName("GUI");
			NodeList guiList = doc.getElementsByTagName("Widget");

			HashMap<String,String> wigitLookup = new HashMap<String,String>();

			for (int i = 0; i < guiList.getLength(); i++) {
				Node gui = guiList.item(i);
				ArrayList<Node> title = new ArrayList<Node>();
				ArrayList<Node> id = new ArrayList<Node>();

				getChildNodeListByNameAndValueRec(gui,"Name","Title", title);

				getChildNodeListByNameRec(gui,"ID", id);

				for (Node c : title){
					//System.out.println(c.getTextContent());
					
					String wigitText= c.getNextSibling().getNextSibling().getTextContent();
					String wigitId = c.getParentNode().getNextSibling().getNextSibling().getLastChild().getPreviousSibling().getTextContent();
					
					wigitLookup.put(wigitText,wigitId);
					System.out.println(c.getNextSibling().getNextSibling().getTextContent());
					System.out.println(c.getParentNode().getNextSibling().getNextSibling().getLastChild().getPreviousSibling().getTextContent());
					System.out.println("----------");
				}
				


			}
				
				for (Entry<String, String> enty :wigitLookup.entrySet()){
					System.out.println(enty.getKey()+":"+enty.getValue());
					
				}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * 
	 * @param ParentNode
	 * @param name
	 * @return Node with the name passes in or null if no Node exists
	 * 
	 * 
	 * 
	 *         private static ArrayList<Node> getChildNodeListByName(Node
	 *         ParentNode, String name) { if (ParentNode == null) { return null;
	 *         } NodeList childernList = ParentNode.getChildNodes(); if
	 *         (childernList == null) { return null; }
	 * 
	 *         ArrayList<Node> nameList = new ArrayList<Node>(); for (int i = 0;
	 *         i < childernList.getLength(); i++) { Node curChild =
	 *         childernList.item(i); if (curChild.getNodeName().equals(name)) {
	 *         nameList.add(curChild); } }
	 * 
	 *         return nameList; }
	 */

	private static ArrayList<Node> getChildNodeListByNameRec(Node ParentNode,
			String name, ArrayList<Node> list) {
		if (ParentNode == null) {
			return null;
		}
		NodeList childernList = ParentNode.getChildNodes();
		if (childernList == null) {
			return null;
		}
		for (int i = 0; i < childernList.getLength(); i++) {
			Node curChild = childernList.item(i);
			if (curChild.hasChildNodes()) {
				getChildNodeListByNameRec(curChild, name, list);
			}
			if (curChild.getNodeName().equals(name)) {
				list.add(curChild);
			}
		}

		return list;
	}


	private static ArrayList<Node> getChildNodeListByNameAndValueRec(Node ParentNode,
			String name,String value, ArrayList<Node> list) {
		if (ParentNode == null) {
			return null;
		}
		NodeList childernList = ParentNode.getChildNodes();
		if (childernList == null) {
			return null;
		}
		for (int i = 0; i < childernList.getLength(); i++) {
			Node curChild = childernList.item(i);
			if (curChild.hasChildNodes()) {
				getChildNodeListByNameAndValueRec(curChild, name,value, list);
			}
			if (curChild.getNodeName().equals(name)){
				if (curChild.getTextContent().equals(value)) {				
					list.add(curChild);
				}
			}
		}

		return list;
	}

	private static HashMap<String, String> getNodeMap(Node ParentNode) {
		HashMap<String, String> map = new HashMap<String, String>();

		if (ParentNode == null) {
			return null;
		}
		NodeList childernList = ParentNode.getChildNodes();
		if (childernList == null) {
			return null;
		}
		for (int i = 0; i < childernList.getLength(); i++) {
			Node curChild = childernList.item(i);

			map.put(curChild.getNodeName(), curChild.getTextContent());
		}

		return map;
	}


}
