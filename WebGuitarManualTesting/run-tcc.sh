#!bin/sh

base_dir=`dirname $0`

main_guitar_dir=$base_dir/..
guitar_lib=$main_guitar_dir/dist/guitar/jars
script_dir=$main_guitar_dir/dist/guitar

tcc_jar=$base_dir/TCC.jar
tcc_buildfile=$base_dir/TestCaseConverter/build-tcc.xml
tcc_build_targets="all"


GUITAR_OPTS="-Dlog4j.configuration=tcc.log4j.properties" 

ripper_width=3
ripper_depth=3



#check that tcc_jar exists
if [ ! -f $tcc_jar ]; then
	echo "[INFO] - $0: TCC.jar does not exist. Building now.."
	echo ""

	ant -f $tcc_buildfile $tcc_build_targets >> /dev/null

	echo "[INFO] - $0: Build complete!"

fi



#create classpath for java calls
for file in `find $guitar_lib -name '*.jar'` 
do
	classpath=$classpath:$file
done
classpath=$classpath:$tcc_jar


placeholder="NO*DIR*SET"

input_dir=$placeholder
output_dir=$placeholder



#functions
function usage()
{
echo "Usage:"
echo "./run-tcc.sh [options]"
echo "	-h --help"
echo "	-d --dir=$input_dir"
echo "	-o --output-dir=$output_dir"
echo ""
echo "Other requirements:"
echo "	GUI & EFG files must be in input directory with proper naming scheme"
echo "		(If these files are missing, they will be created)"
echo ""
}


function cleanUp()
{
#cleanup
echo "[INFO] - $0: Running cleanup.."
echo ""
rm -rf $base_dir/*.log
rm -rf $output_dir/*.log
rm -rf $base_dir/log_widget.xml
rm -rf $output_dir/log_widget.xml


}






#argument handling


echo "[INFO] - $0: Launching argument parsing.."
echo "" 
while [ "$1" != "" ]; do
PARAM=$1
VALUE=$2
echo "	PARAM: $PARAM"
echo "	VALUE: $VALUE"
echo "- - - -"
	case $PARAM in
	-h | --help)
	usage
	exit
	;;
	-d | --dir)
	input_dir=$VALUE
	;;
	-o | --output-dir)
	output_dir=$VALUE
	;;
	*)
	echo "ERROR: unknown parameter \"$PARAM\""
	usage
	exit 1
	;;
	esac
shift
shift
done

if [ $input_dir = $placeholder ]; then
	input_dir="."
fi


if [ $input_dir = "." ]; then

	input_dir=$(readlink -f .)

fi


if [ $output_dir = $placeholder ]; then
	output_dir=$input_dir
fi




#create GUI and EFG files

echo "[INFO] - $0: Searching for testcases.."
echo ""

count=0

for file in `find $input_dir -name '*.java'`
do

let count+=1

# getting test name 
  test_name=`basename $file`
  test_name=${test_name%.*}

# getting name from naming scheme

cmd="java"

#extract URL using SeleniumJUnitParser
main_class=SeleniumJUnitParser
domain=extraction

url=$($cmd -cp $classpath $domain.$main_class $file)




main_class=NamingScheme
domain=naming
#get name from namingscheme, store in $filename
filename=$($cmd -cp $classpath $domain.$main_class $url)

echo "[INFO] - $0: Testcase parsed.."
echo "[INFO] - $0:	URL => $url"
echo "[INFO] - $0:	$test_name => $filename"
echo ""

piped_output=$output_dir/$filename.out
piped_err=$output_dir/$filename.err



gui_file=$input_dir/$filename.GUI
efg_file=$input_dir/$filename.EFG


if [ ! -f $gui_file ]; then
	echo "[INFO] - $0: Ripping URL.."
	#rip
	bash $script_dir/sel-ripper.sh --website-url $url -g $gui_file -w $ripper_width -d $ripper_depth >> $piped_output 2>> $piped_err

else

	echo "[INFO] - $0: $filename.GUI present, skipping GUI creation."

fi


if [ ! -f $efg_file ]; then
	echo "[INFO] - $0: Creating EFG.."
	#gui-to-efg
	bash $script_dir/gui2efg.sh -g $gui_file -e $efg_file >> $piped_output 2>> $piped_err

else
	echo "[INFO] - $0: $filename.EFG present, skipping EFG creation."

fi


done


if [ $count -eq 0 ]; then

	echo "[ERROR] - $0: No testcases found!"
	echo ""
	cleanUp
	exit 1

fi


#call TCC

cmd="java"
main_class=TCCLauncher
domain=launcher

echo "Running TCC!"
#echo "	input-dir:  $input_dir"
#echo "	output-dir: $output_dir"

$cmd $GUITAR_OPTS -cp $classpath $domain.$main_class -d $input_dir -o $output_dir


cleanUp










