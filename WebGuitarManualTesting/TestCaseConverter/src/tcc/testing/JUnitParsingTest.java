package tcc.testing;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import extraction.SeleneseSteps;
import extraction.SeleneseWrapper;
import extraction.SeleniumJUnitParser;

public class JUnitParsingTest {

	@Test
	public void test() {
		String inputPath = "/home/cluster/.jenkins/workspace/WebGuitarJobGIT/WebGuitar/WebGuitarManualTesting/textTest.java";
		
		File f = new File(inputPath);
		WebDriver driver = new FirefoxDriver();
		SeleneseSteps steps = SeleniumJUnitParser.parseJUnitFile(f, driver);
		driver.quit();
		
		for(SeleneseWrapper step : steps.getSteps()){
			
			System.out.println("- - - -");
			System.out.println("TAG: " +step.getElement().getTagName());
			System.out.println("ACTION: " + step.getSeleniumAction().name());
			
		}
		
		
	}

}
