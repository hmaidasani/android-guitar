package simplified;

import java.util.List;

import edu.umd.cs.guitar.model.GUITARConstants;
import edu.umd.cs.guitar.model.data.GUIStructure;
import edu.umd.cs.guitar.model.wrapper.ComponentTypeWrapper;
import edu.umd.cs.guitar.model.wrapper.EventWrapper;
import edu.umd.cs.guitar.model.wrapper.GUIStructureWrapper;
import edu.umd.cs.guitar.model.wrapper.GUITypeWrapper;

/**
 * 
 * Code copied/modified from guistructure2graph-efg/src/edu/umd/cs/guitar/graph/plugin/EFGConverter.java
 * 
 */
public class GUICrawler {

	
	
	
	
	
	
	
	public static void crawl(GUIStructure gui, SimplifiedGUI simple ){
		
		
		
		GUIStructure dGUIStructure = gui;

	      dGUIStructure.getGUI().get(0).getContainer().getContents()
	            .getWidgetOrContainer();
	      GUIStructureWrapper wGUIStructure = new GUIStructureWrapper(
	            dGUIStructure);

	      wGUIStructure.parseData();

	      List<GUITypeWrapper> wWindowList = wGUIStructure.getGUIs();

	      for (GUITypeWrapper window : wWindowList) {
	         readEventList(window.getContainer(), simple);
	      }
		
			
		
	}
	
	private static final String EVENT_ID_SPLITTER = "_";
	   /**
	     * 
	     */
	   private static final String EVENT_ID_PREFIX = "e";
	
	private static void readEventList(ComponentTypeWrapper component, SimplifiedGUI simple) {

	      List<String> sActionList = component
	            .getValueListByName(GUITARConstants.EVENT_TAG_NAME);

	      if (sActionList != null)
	         for (String action : sActionList) {
	            EventWrapper wEvent = new EventWrapper();

	            // Calculate event ID
	            String sWidgetID = component
	                  .getFirstValueByName(GUITARConstants.ID_TAG_NAME);

	            String noPrefixsWidgetID = sWidgetID.substring(1);

	            String sEventID = EVENT_ID_PREFIX + noPrefixsWidgetID;

	            String posFix = (sActionList.size() <= 1) ? ""
	                  : EVENT_ID_SPLITTER
	                        + Integer.toString(sActionList.indexOf(action));
	            sEventID = sEventID + posFix;

	            wEvent.setID(sEventID);
	            wEvent.setAction(action);
	            wEvent.setComponent(component);
	            wEvent.setListeners(component
	                  .getValueListByName("ActionListeners"));
	            
	            
	            String x = component.getFirstValueByName(GUITARConstants.X_TAG_NAME);
	            String y = component.getFirstValueByName(GUITARConstants.Y_TAG_NAME);
	            
	            String title = component.getFirstValueByName(GUITARConstants.TITLE_TAG_NAME);
	            
	            /*
	            
	            System.out.println("- - - -");
	            System.out.println("WidgetID: " + sWidgetID);
	            System.out.println("Tag: " + title);
	            System.out.println("X: " + x);
	            System.out.println("Y: "+y);
	            
	            */
	            
	            //add to simple gui
	            simple.addGUIElement(sWidgetID, title, Integer.parseInt(x), Integer.parseInt(y));
	            
	          
	            
	            
	         }

	      List<ComponentTypeWrapper> wChildren = component.getChildren();
	      if (wChildren != null)
	         for (ComponentTypeWrapper wChild : wChildren) {
	            readEventList(wChild,simple);
	         }
	   }
	
	
	
}
