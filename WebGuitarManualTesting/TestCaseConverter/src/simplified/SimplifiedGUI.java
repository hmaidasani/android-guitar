package simplified;

import java.util.ArrayList;
import java.util.List;

import simplified.data.SimplifiedGUIElement;

public class SimplifiedGUI {

	List<SimplifiedGUIElement> guiElements;
	
	
	public SimplifiedGUI(){
		guiElements = new ArrayList<SimplifiedGUIElement>();
	}
	
	
	public void addGUIElement(String widgetID, String tagName, int x, int y){
		SimplifiedGUIElement simpleElem = new SimplifiedGUIElement(widgetID,tagName,x,y);
		
		addGUIElement(simpleElem);
	}
	
	
	public void addGUIElement(SimplifiedGUIElement simpleElem){
		guiElements.add(simpleElem);
	}
	
	
	public List<SimplifiedGUIElement> getGUIElements(){
		return guiElements;
	}
	
}
