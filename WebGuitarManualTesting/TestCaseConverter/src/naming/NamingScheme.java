package naming;

public class NamingScheme {
	
	
	
	
	public static void main(String[] args) {
		
		if(args.length >= 1)
			System.out.print(getName(args[0]));
		else
			System.out.print("no_arg");
		
		
	}
	
	public static String getName(String str){
		
		return namingSchemeOne(str);
		
	}
	
	
	
	private static String namingSchemeOne(String str){
		
		return Integer.toHexString(str.hashCode());
		
		
	}

}
