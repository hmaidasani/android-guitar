package extraction;

public class InvalidSeleniumActionThrowable extends Throwable{
	
	
	private static final long serialVersionUID = -6417170106747719237L;

	public InvalidSeleniumActionThrowable(){
		super();
	}
	
	public InvalidSeleniumActionThrowable(String msg){
		super(msg);
	}

}
