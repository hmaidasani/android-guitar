package extraction;

import java.util.List;

public class SeleneseSteps {
	
	
	private List<SeleneseWrapper> steps;
	private String url;
	
	private String name;
	
		
	private String efg = null;
	private String gui = null;
	
	
	public SeleneseSteps(List<SeleneseWrapper> stepsList, String url, String name){
		this.url = url;
		this.steps = stepsList;
		this.name = name;
		
	}
	
	public String getName(){
		return name;
	}
	
	
	public List<SeleneseWrapper> getSteps(){
		
		return steps;
		
	}
	
	public String getURL(){
		return url;
	}
	
	
	public String getEFG(){
		return efg;
	}
	
	public String getGUI(){
		return gui;
	}
	
	public void setEFG(String efg){
		this.efg = efg;
	}
	
	public void setGUI(String gui){
		this.gui = gui;
	}
	
	@Override
	public String toString(){
		String str="";
		
		str += "NAME: " + name + "\n";
		str += "URL:  " + url + "\n";
		str += "GUI:  " + (gui == null ? "null" : gui) + "\n";
		str += "EFG:  " + (efg == null ? "null" : efg) + "\n";
		str += "STEPS: \n";
		
		for(SeleneseWrapper step : steps){
			
			str += "+STEP: \n";
			str += step.toString() + "\n";
			
			
		}
		
		
		
		
		return str;
		
	}
	
	
	
}
