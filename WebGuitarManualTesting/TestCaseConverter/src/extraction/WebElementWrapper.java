package extraction;

import org.openqa.selenium.WebElement;

public class WebElementWrapper {

	
	private WebElement webElem;
	
	private String tagName;
	private String text;
	private int x;
	private int y;
	
	
	
	
	public WebElementWrapper(WebElement wE){
		
		
		webElem = wE;

		
		tagName = new String(webElem.getTagName());//deep copy
		text = new String(webElem.getText()); //deep copy
		x = webElem.getLocation().getX();
		y = webElem.getLocation().getY();
		
		
		
	}
	
	
	public String getTagName(){
		return tagName;
	}
	
	public String getText(){
		return text;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;		
	}
	
	public WebElement getWebElement(){
		return webElem;		
	}
	
	
}
