package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.openqa.selenium.WebDriver;

public class SeleniumJUnitParser {
	
	private static String findElementPattern = "^.*findElement\\(By\\.(\\w+)\\(\"([=\\(\\)@\\.\\/a-zA-Z_0-9\\'\\[\\]]+)\"\\)\\)\\.(\\w+)\\((.*)\\);.*$";
	private static String getURLPattern = "^\\s*driver\\.get\\(baseUrl\\s*\\+\\s*\"(.+)\"\\);.*$";
	private static String baseURLPattern="^\\s*baseUrl\\s*=\\s*\"(.*)\";\\s*$";
	
	
	public static void main(String[] args){
		
		
		if(args.length >= 1){
			String fileName = args[0];
			
			File f = new File(fileName);
			
			System.out.print(getUrl(f));
			
		} else System.out.print("no_args");
		
		
	}
	
	
	private static String getUrl(File junitFile){
		
		String baseURL = "https://www.google.com/search?q=failed+to+get+baseurl&oq=failed+to+get+baseurl";
		String url = "+failed+to+get+url";
				
		boolean baseURLSet = false;
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(junitFile));
			
			String line;
			while ((line = br.readLine()) != null) {
			
			
				
				if(line.matches(baseURLPattern)){
				
					baseURL = line.replaceFirst(baseURLPattern, "$1");
					
					baseURLSet = true;
					
				} else if(line.matches(getURLPattern)){
					
					url = line.replaceFirst(getURLPattern, "$1");
					
	
					if(baseURLSet){
						
						return baseURL+url;
						
					}
				}
			}
			
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return baseURL;
	}
	
	
	public static SeleneseSteps parseJUnitFile(File junitFile, WebDriver driver){
		
		
		String baseURL = "https://www.google.com/search?q=failed+to+get+baseurl&oq=failed+to+get+baseurl";
		String url = "+failed+to+get+url";
		
		String fileName = junitFile.getName();
		String name = fileName.replaceFirst("[.][^.]+$","");
		
		
		boolean baseURLSet = false;
		
		WebElementFetcher fetcher = null;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(junitFile));
			
			String line;
			
			
			
			while ((line = br.readLine()) != null) {

				line = line.replace("\\\"", "@PLACEHOLDER@");
				
				
				if(line.matches(baseURLPattern)){
					
					baseURL = line.replaceFirst(baseURLPattern, "$1");
					
					baseURLSet = true;
					
				} else if(line.matches(getURLPattern)){
					
					url = line.replaceFirst(getURLPattern, "$1");
					
	
					if(baseURLSet){
						
						fetcher = new WebElementFetcher(driver,baseURL + url);
						
					}
					
					
					
					
				} else if(line.matches(findElementPattern)){
					
					
					if(fetcher != null) {
					
						String byStr = line.replaceFirst(findElementPattern, "$1");
						String locator = line.replaceFirst(findElementPattern, "$2");
						locator = locator.replace("@PLACEHOLDER@", "\"");
						String action = line.replaceFirst(findElementPattern, "$3");
						String actionParam = line.replaceFirst(findElementPattern, "$4");
						
						
						//debug
						/*
						System.out.println("- - - - -");
						System.out.println("BY: " + byStr);
						System.out.println("LOCATOR: " + locator);
						System.out.println("ACTION: " + action);
						System.out.println("ACTION-PARAM: " + actionParam);
						*/
						
						
						fetcher.addElement(byStr, locator, action, actionParam);
					
					}
					
				}
				
				
				
				
			}
			br.close();
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(fetcher == null){
			return null;
		} else {
			
			return new SeleneseSteps(fetcher.getSeleneseSteps(), baseURL + url, name);
			
		}
		
	}
	
	

}
