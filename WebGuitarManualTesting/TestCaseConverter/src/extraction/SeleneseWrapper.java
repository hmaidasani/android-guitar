package extraction;

import org.openqa.selenium.WebElement;

import edu.umd.cs.guitar.model.data.EventType;

//import edu.umd.cs.guitar.model.data.EventType;

public class SeleneseWrapper {
	
	private WebElementWrapper webElem;
	private String actionStr;
	private String actionParam;
	
	private SeleniumAction action;
	
	
	//extracted from GUI file
	private String widgetID = "n/a";	//default value
	
	
	private EventType eventPair = null;
	
	public SeleneseWrapper(WebElement wE, String action, String actionParam){
		this.webElem = new WebElementWrapper(wE);
		this.actionStr = action;
		this.actionParam = actionParam.replaceFirst("\"(.*)\"", "$1");//pull string out of quotes
		
		try {
			this.action = SeleniumAction.getAction(actionStr);
			
		} catch (InvalidSeleniumActionThrowable e) {
			e.printStackTrace();
		}
		
		
		//debug
		/*
		System.out.println("- - - -");
		System.out.println("SeleneseWrapper Created:");
		System.out.println("\tAction: " + this.action.name());
		 */
		
		
	}
	
	
	private String webElementToString(){
		String str ="";
		
		str += "\t" + "WEB-ELEMENT:" +"\n";
		if(webElem == null){
			str += "\tnull\n";
		} else {
		str += "\t\t" + "TAG-NAME: " + webElem.getTagName() + "\n";
		str += "\t\t" + "TEXT:     " + webElem.getText().toString() + "\n";
		str += "\t\t" + "COORDS:   " + "("+ webElem.getX() + ", "+ webElem.getY() + ")"+ "\n";
		}
		
		
		
		return str;
	}
	
	
	@Override
	public String toString(){
		String str = "";
		
		str += "\t" + "WIDGET-ID:    " + widgetID + "\n";
		str += "\t" + "ACTION:       " + actionStr + "\n";
		str += "\t" + "ACTION-PARAM: " + actionParam + "\n";
		
		str += webElementToString() + "\n";
		
		
		
		return str;
		
	}

	
	public WebElementWrapper getElement(){
		return webElem;
	}
	
	public String getAction(){
		return actionStr;
	}
	
	public SeleniumAction getSeleniumAction(){
		return action;
	}
	
	public String getActionParam(){
		return actionParam;
	}
	
	
	public String getID(){
		return widgetID;
	}
	
	public void setID(String newId){
		this.widgetID = newId;
	}
	
	
	
	public String getEventID(){
		return eventPair.getEventId();
	}
	
	

	public void setEvent(EventType event){
		this.eventPair = event;
	}

	
	public EventType getEvent(){
		return eventPair;
	}
	
	public boolean isReachingStep(){
		
		return eventPair == null ? false : !eventPair.isInitial();
		
	}
	
	
	
}
