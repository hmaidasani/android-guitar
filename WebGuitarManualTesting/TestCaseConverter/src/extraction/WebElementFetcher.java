package extraction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebElementFetcher {
	
	
	 private WebDriver driver;
	 private String url;
	 private List<SeleneseWrapper> webElements;
	
	public WebElementFetcher(WebDriver driver, String url){
		this.driver = driver;
	    this.url = url;
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	    webElements = new ArrayList<SeleneseWrapper>();
		
	}
	
	
	
	public List<SeleneseWrapper> getSeleneseSteps(){
		
		return webElements;
		
	}

	public void addElement(String by, String locator, String action,
			String actionParam) {
		driver.get(url);//open webpage		
		
		By byObj = bySelector(by,locator);
		
		if(byObj == null){
			throw new NullPointerException("Not valid 'by' call");
		}
		
		//System.out.println("Fetching element using: By." +by +"(\""+locator+"\")" );//debug
		WebElement wE = driver.findElement(byObj);
		
		if(wE == null){
			throw new NullPointerException("WebElement returned null!");
		} else {
			//System.out.println("\tElement fetched."); //debug
		}
		
		SeleneseWrapper step = new SeleneseWrapper(wE,action,actionParam);
		
		webElements.add(step);
		//update webdriver
		performAction(step);
		

	}
	
	private void performAction(SeleneseWrapper step){
		
		WebElementWrapper wrapper = step.getElement();
		
		WebElement wE = wrapper.getWebElement();
		SeleniumAction action = step.getSeleniumAction();
		String param = wrapper.getText();
		
		SeleniumAction.perform(action, wE, param);
		
		
	}
	

	private By bySelector(String byStr, String locator) {

		if (byStr.equalsIgnoreCase("name")) {

			return By.name(locator);

		} else if (byStr.equalsIgnoreCase("cssSelector")) {

			return By.cssSelector(locator);
			
		} else if(byStr.equalsIgnoreCase("xpath")){
			
			return By.xpath(locator);
			
		} else if(byStr.equalsIgnoreCase("id")){
			
			return By.id(locator);
			
		} else if(byStr.equalsIgnoreCase("linkText")){
			
			return By.linkText(locator);
			
		} else if(byStr.equalsIgnoreCase("tagName")){
			
			return By.tagName(locator);
			
		} else if(byStr.equalsIgnoreCase("partialLinkText")){
			
			return By.partialLinkText(locator);
			
		} else return null;

	}

}
