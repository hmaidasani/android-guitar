package generation;

public class TstStep {


	private String eventID;
	

	private boolean reachingStep;
	
	public TstStep(String id, boolean reaching){
		this.eventID = id;
		this.reachingStep = reaching;
	}
	
	
	public String getID(){
		return eventID;
	}
	
	public boolean getReachingStep(){
		return reachingStep;
	}
	
	
}
