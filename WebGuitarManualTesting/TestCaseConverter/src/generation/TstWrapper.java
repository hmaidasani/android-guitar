package generation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import launcher.Util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.umd.cs.guitar.model.GUITARConstants;
import edu.umd.cs.guitar.model.XMLHandler;
import edu.umd.cs.guitar.model.data.AttributesType;
import edu.umd.cs.guitar.model.data.PropertyType;
import edu.umd.cs.guitar.model.data.StepType;
import edu.umd.cs.guitar.model.data.TestCase;
import extraction.SeleneseSteps;
import extraction.SeleneseWrapper;
import extraction.SeleniumAction;

public class TstWrapper {
	
	private SeleneseSteps steps;
	private String outputDirectory;
	private String name;
	
	
	public TstWrapper(SeleneseSteps steps, String outputDirectory, String name){
		
		this.steps = steps;
		this.outputDirectory = outputDirectory;
		this.name = name;
	}
	
	
	public void generate(){
		
		XMLHandler xml = new XMLHandler();
		
		TestCase tst = new TestCase();
		List<StepType> stepList = new ArrayList<StepType>();
		String outputPath = outputDirectory + "/" + name + ".tst";
		
		for(SeleneseWrapper step :steps.getSteps()){
			
			StepType s = new StepType();
			
			s.setEventId(step.getEventID());
			s.setReachingStep(step.isReachingStep());
			
			if(step.getSeleniumAction() == SeleniumAction.TYPE){
				
				AttributesType attr = new AttributesType();
				PropertyType prop = new PropertyType();
				List<String> values = new ArrayList<String>();
				values.add(step.getActionParam());
				prop.setName(GUITARConstants.INPUT_VALUE_PROPERTY_TYPE);
				prop.setValue(values);
				List<PropertyType> props = new ArrayList<PropertyType>();
				props.add(prop);
				attr.setProperty(props);
				s.setOptional(attr);		
				
			}
			
			stepList.add(s);
			
		}
		
		tst.setStep(stepList);
		
		xml.writeObjToFile(tst, outputPath);
		
		
	}
	
	/*
	// DOM approach
	public void generate1(){
		
		Util.printInfo("Generating .tst for " + steps.getName()+".. ");
	
		Tst tst = new Tst();
		String outputPath = outputDirectory + "/" + name + ".tst";
		
		for(SeleneseWrapper step : steps.getSteps()){
			
			TstStep tStep = new TstStep(step.getEventID(),step.isReachingStep());
			tst.addStep(tStep);
			
		}
		
		

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			
			
			
			Element rootTag = doc.createElement("TestCase");
			doc.appendChild(rootTag);
			
			
			for(TstStep step : tst.steps){
				
				Element stepTag = doc.createElement("Step");
				
				Element eIDTag = doc.createElement("EventId");
				eIDTag.setTextContent(step.getID());
				
				Element reachingTag = doc.createElement("ReachingStep");
				reachingTag.setTextContent(step.getReachingStep() ? "true" : "false");
				
				stepTag.appendChild(eIDTag);
				stepTag.appendChild(reachingTag);
				
				rootTag.appendChild(stepTag);
			}
			
			
			launcher.Util.transform(outputPath, doc);
			
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
	}

	*/

}
