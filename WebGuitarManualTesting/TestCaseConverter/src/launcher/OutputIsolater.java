package launcher;

import java.io.OutputStream;
import java.io.PrintStream;

public class OutputIsolater {

	PrintStream out;
	PrintStream err;
	
	PrintStream devNull;
	
	
	public OutputIsolater(){
		this.out = System.out;
		this.err = System.err;
		
		devNull = new PrintStream(new OutputStream(){
			public void write(int b){
				//do nothing
			}
		});
		
	}
	
	public void isolate(){
		System.setOut(devNull);
		System.setErr(devNull);
	}
	
	public void revert(){
		System.setOut(out);
		System.setErr(err);
	}
	
	
	
}
