package launcher;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class Util{
	
	public static final String PROJECT_NAME = "TCC";
	
	
	static public String getTimestamp(){
		Date d = new Date();
		return new Timestamp(d.getTime()).toString();	
	}
	
	/**
	 * Safe for filename
	 * @return
	 */
	static public String getSafeTimestamp(){
		String timestamp = getTimestamp();
		
		timestamp = timestamp.replace(' ', '.');
		timestamp = timestamp.replace(':', '.');
		
		return timestamp;
	}
	
	
	public static void transform(String outputPath, Document doc){
		
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			Result output = new StreamResult(new File(outputPath));
			Source input = new DOMSource(doc);

			transformer.transform(input, output);
			//System.out.println(outputPath + " created!");
			
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void printError(String msg){
		
		System.err.println("[ERROR] - "+PROJECT_NAME+": " + msg);
	}
	
	public static void printWarning(String msg){
		
		System.err.println("[WARNING] - "+PROJECT_NAME+": " + msg);
	}
	
	
	
	public static void printInfo(String msg){
		
		System.out.println("[INFO] - "+PROJECT_NAME+": " + msg);
	}
}
