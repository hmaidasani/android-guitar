package launcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import naming.NamingScheme;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import simplified.GUICrawler;
import simplified.SimplifiedGUI;
import simplified.data.SimplifiedGUIElement;

import edu.umd.cs.guitar.model.GUITARConstants;
import edu.umd.cs.guitar.model.XMLHandler;
import edu.umd.cs.guitar.model.data.AttributesType;
import edu.umd.cs.guitar.model.data.EFG;
import edu.umd.cs.guitar.model.data.EventType;
import edu.umd.cs.guitar.model.data.EventsType;
import edu.umd.cs.guitar.model.data.GUIStructure;
import edu.umd.cs.guitar.model.data.PropertyType;
import extraction.SeleneseSteps;
import extraction.SeleneseWrapper;
import extraction.SeleniumAction;
import extraction.SeleniumJUnitParser;
import extraction.WebElementWrapper;
import generation.TstWrapper;

/**
 * 
 * Converts Selenium JUnit testcases into .tst files
 * 
 * 
 * @author Thomas Irish
 *
 */
public class TCCLauncher {

	private static final String POSTFIX = "_s";
	
	/**
	 * Handle argument processing
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		BasicParser parser = new BasicParser();

		try {
			CommandLine cmd = parser.parse(getOptions(), args);

			String directory = cmd.getOptionValue("d");


			String outputDir = directory; // default

			if (cmd.hasOption("o")) {

				outputDir = cmd.getOptionValue("o");

			}
			
			
			if(outputDir == null || outputDir.equals("null"))
				outputDir=directory;
			

			TCCLauncher processer = new TCCLauncher(directory,
					outputDir);

			Util.printInfo("Arguments parsed:");
			Util.printInfo("\tInput directory= " + directory);
			Util.printInfo("\tOutput directory= " + outputDir);
			
			
			
			// run it
			processer.process();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static Options getOptions() {
		Options options = new Options();

		options.addOption(new Option("d", "dir", true,"Directory to fetch input JUnit file(s)"));
		options.addOption(new Option("o", "output-dir", true,"Output directory for .tst file(s)"));

		return options;
	}

	private String inputDirectory;
	private String outputDirectory;
	
	private OutputIsolater isolater = new OutputIsolater();
	

	private int repairCounter = 0;

	public TCCLauncher(String inputDirectory,
			String outputDirectory) {
		this.inputDirectory = inputDirectory;
		this.outputDirectory = outputDirectory;

	}

	public void process() {
		
		
		

		XMLHandler xmlHandler = new XMLHandler();
		
		TreeMap<String, SeleneseSteps> testMap = new TreeMap<String, SeleneseSteps>();

		TreeMap<String, ReferenceFileWrapper> urlReferenceFiles = new TreeMap<String, ReferenceFileWrapper>();

		
		setUp();
		
		Util.printInfo("About to start processing JUnit files..");
		
		/*
		 * Fetch the Selenese steps for each input file
		 */
		File dir = new File(inputDirectory);

		int count = 0;
		for (File child : dir.listFiles()) {

			if (child.isFile() && child.getName().endsWith(".java")) {
				count++;
				// process .java files from input directory

				WebDriver driver = new FirefoxDriver();//debug
				
				Util.printInfo("About to process " + child.getName());
				// parse selenese steps
				SeleneseSteps steps = SeleniumJUnitParser.parseJUnitFile(child,
						driver);
				isolater.isolate();
				driver.quit();//debug
				isolater.revert();
				
				//System.out.println(child.getName() +" parsed!");
				//System.exit(0);
				

				String str = child.getName();
				String name = str;
				if (str.contains("."))
					name = str.substring(0, str.lastIndexOf('.'));

				String url = steps.getURL();

				// if URL does not have reference files set, create them
				if (!urlReferenceFiles.containsKey(url)) {

					ReferenceFileWrapper files = new ReferenceFileWrapper();
					
					
					//name gui/efg using hashcode of the url
					
					String filename = NamingScheme.getName(url);
					
					files.gui = inputDirectory+"/"+filename+".GUI";
					//files.gui = createGUI(url, filename); // creates GUI file,
														// returns path		
					
					files.efg = inputDirectory +"/"+filename+".EFG";
					//files.efg = createEFG(files.gui, filename); // creates EFG file,
															// returns path
					boolean fail = false;
					if(!new File(files.gui).exists()){
						Util.printError(files.gui + " does not exist!");
						fail = true;
					}

					if(!new File(files.efg).exists()){
						Util.printError(files.efg + " does not exist!");
						fail = true;
					}
					
					if(fail)
						continue;
					
					
					//create conversion info file
					Util.printInfo("Saving test info to info-object..");
					files.info = new AdditionalInfoFile(url, filename);
					
					
					urlReferenceFiles.put(url, files);
					
					
				}

				ReferenceFileWrapper files = urlReferenceFiles.get(url);

				steps.setGUI(files.gui);
				steps.setEFG(files.efg);
				files.info.addTestCase(name);
				

				// store the parsed selenese steps using the name of the .java
				// file (without extension)
				SeleneseSteps overwrite = testMap.put(name, steps);

				if (overwrite != null)
					Util.printWarning("Something went wrong. More than one manual testcase has been mapped to the same name! All but one will be lost");

			}

		}
		
		
		if(count == 0){
			Util.printWarning("No .java files found in input directory!");
			Util.printWarning("Program now exiting!");
			System.exit(1);
		}
		
		
		
		/*
		 * Save Info files
		 */
		Util.printInfo("Creating info file(s)");
		for(ReferenceFileWrapper files : urlReferenceFiles.values()){
			
			AdditionalInfoFile infoFile = files.info;
			
			String outputPath = outputDirectory + "/" + infoFile.filename + ".info";
			
			//xmlHandler.writeObjToFile(infoFile, outputPath);
			
			InfoFileWriter.write(infoFile, outputPath);
			
			Util.printInfo(infoFile.filename + ".info created!");
			
		}
		
		
		

		/*
		 * Use Selenese steps to create tst files for each "SeleneseSteps" obj
		 */

		
		for (Map.Entry<String, SeleneseSteps> ent : testMap.entrySet()) {

			

			String name = ent.getKey();
			SeleneseSteps steps = ent.getValue();
			// String url = steps.getURL();

			Util.printInfo("Pairing " +steps.getName() + " with GUI & EFG..");
			
			List<SeleneseWrapper> stepsList = steps.getSteps();

			// iterate through the steps of this Selenium testcase
			for (SeleneseWrapper seleneseStep : stepsList) {

				pairWithGUI(seleneseStep, steps, xmlHandler);

				// at this point, SeleneseWrapper object should have a WidgetID
				// set, pairing with GUI file

				// proceed to pair with EFG events

				pairWithEFG(seleneseStep, steps, xmlHandler);

				// at this point, SeleneseWrapper object have a

			}// end foreach SeleneseWrapper obj should have a WidgetID set and
				// an EventID set, pairing with both GUI and EFG files

			// at this point, ALL SeleneseWrapper objects should have a WidgetID
			// set and EventID set, pairing them with the GUI and EFG files

			// proceed to create tst file

			TstWrapper tWrap = new TstWrapper(steps, outputDirectory, name);
			tWrap.generate();

		}// end foreach SeleneseSteps obj

	}// end process()
	
	
	
	
	
	private void setUp() {
		
		Util.printInfo("Executing program setup..");
		
		
		Util.printInfo("Cleaning output directory..");
		// clean out outputdirectory
		File dir = new File(outputDirectory);
		
		
		File[] files = dir.listFiles();
		
		for (int i = 0; i < files.length; i++) {
			File child = files[i];
			
			String name = child.getName();
			
			if(name.endsWith(".info") || name.endsWith(".tst"))
				child.delete();
			
			
		}
		
		
		
	}

	

	/**
	 * Find Event from EFG that corresponds to the SeleneseWrapper, and set the
	 * Event within the SeleneseWrapper
	 * 
	 * 
	 * @param seleneseStep
	 * @param steps
	 * @param xmlHandler
	 */
	private void pairWithEFG(SeleneseWrapper seleneseStep, SeleneseSteps steps,
			XMLHandler xmlHandler) {
		isolater.isolate();
		
		String efgPath = steps.getEFG();
		EFG efg = (EFG) xmlHandler.readObjFromFile(efgPath, EFG.class);

		for (EventType event : efg.getEvents().getEvent()) {
			
			if(skipCustomizedEvent(event))
				continue;
			

			if (isMatched(seleneseStep, event)) {

				// repair/update EFG if needed

				if (needsRepair(seleneseStep, event)) {
					
					isolater.revert();
					Util.printInfo("EFG needs repair for " + steps.getName() +" ("+seleneseStep.getSeleniumAction().toString()+")");
					isolater.isolate();
					
					SeleniumAction actionType = seleneseStep.getSeleniumAction();
					
					if (actionType == SeleniumAction.TYPE || actionType == SeleniumAction.CLEAR){
						event = handleCustomText(efg, seleneseStep, event,
								efgPath, xmlHandler);
					}
					
					//reload efg
					efg = (EFG) xmlHandler.readObjFromFile(efgPath, EFG.class);

				}

				seleneseStep.setEvent(event);
				break;
			}

		}
		
		isolater.revert();

	}

	
	
	/**
	 * Returns true if the event is one that has been newly created by this program
	 * 
	 * 
	 * @param event
	 * @return
	 */
	private boolean skipCustomizedEvent(EventType event) {
		
		String eID = event.getEventId();
		
		boolean checkOne = eID.contains(POSTFIX);
		
		return checkOne;
	}

	/**
	 * 
	 * Check if EFG needs to be updated.
	 * 
	 * Currently checks: + Is seleneseStep referring to typing custom text into
	 * a text field
	 * 
	 * @param seleneseStep
	 * @param event
	 * @return
	 */
	private boolean needsRepair(SeleneseWrapper seleneseStep, EventType event) {

		boolean checkOne = seleneseStep.getSeleniumAction() == SeleniumAction.TYPE;
		boolean checkTwo = seleneseStep.getSeleniumAction() == SeleniumAction.CLEAR;

		return checkOne || checkTwo;
	}

	/**
	 * 
	 * Creates a new EventType obj with a modified EventID and added optional data. Updates the EFG's data, then rewrites the EFG file.
	 * 
	 * 
	 * @param efg
	 * @param seleneseStep
	 * @param event
	 * @param efgPath
	 * @param xmlHandler
	 * @return
	 */
	private EventType handleCustomText(EFG efg, SeleneseWrapper seleneseStep,
			EventType event, String efgPath, XMLHandler xmlHandler) {

		EventType newEvent = new EventType();

		//create new eventID based on the old one, but with a custom postfix
		String newEID = event.getEventId() + POSTFIX
				+ Integer.toHexString(repairCounter++);

		// handle optional data
		AttributesType attr = new AttributesType();
		List<PropertyType> properties = new ArrayList<PropertyType>();
		PropertyType prop = new PropertyType();
		List<String> values = new ArrayList<String>();
		values.add(seleneseStep.getActionParam());
		prop.setName(GUITARConstants.INPUT_VALUE_PROPERTY_TYPE);
		prop.setValue(values);

		properties.add(prop);
		attr.setProperty(properties);

		// new data
		newEvent.setEventId(newEID);
		newEvent.setOptional(attr);

		// old data
		newEvent.setAction(event.getAction());
		newEvent.setInitial(event.isInitial());
		newEvent.setListeners(event.getListeners());
		newEvent.setName(event.getName());
		newEvent.setType(event.getType());
		newEvent.setWidgetId(event.getWidgetId());

		// update efg
		EventsType allEvents = efg.getEvents();

		List<EventType> eventsList = allEvents.getEvent();

		eventsList.add(newEvent);

		allEvents.setEvent(eventsList);

		efg.setEvents(allEvents);

		File file = new File(efgPath);

		file.delete();

		Util.printInfo("Rewriting EFG file with custom event..");
		
		//rewrite the EFG file
		xmlHandler.writeObjToFile(efg, efgPath);

		return newEvent;
	}

	/**
	 * 
	 * Finds the GUI file element that corresponds to the SeleneseWrapper, and
	 * sets the SeleneseWrapper's ID to be the ID of that GUI file element
	 * 
	 * @param seleneseStep
	 * @param steps
	 * @param xmlHandler
	 */
	private void pairWithGUI(SeleneseWrapper seleneseStep, SeleneseSteps steps,
			XMLHandler xmlHandler) {
		

		WebElementWrapper elementUsed = seleneseStep.getElement();

		String guiPath = steps.getGUI();

		GUIStructure gui = (GUIStructure) (xmlHandler.readObjFromFile(guiPath,
				GUIStructure.class));
		
		SimplifiedGUI simpleGUI = new SimplifiedGUI();
		
		GUICrawler.crawl(gui, simpleGUI);
		
		
		

		// find element in gui file
		for (SimplifiedGUIElement guiElem : simpleGUI.getGUIElements()) {
			
			if (isMatched(elementUsed, guiElem)) {

				// set id for selenese step (taken from GUI file)
				String id = guiElem.getWidgetID();
				seleneseStep.setID(id);
				break;
			}// end if

		}// end foreach GUIType (from GUI file)

	}

	
	/**
	 * Tests if the WebElement and GUI file element are matched (refer to the
	 * same thing).
	 * 
	 * Currently does the following checks: + Check X coordinate + Check Y
	 * coordinate + Check tag name
	 * 
	 * @param wE
	 * @param simpleGE
	 * @return
	 */
	private boolean isMatched(WebElementWrapper wE, SimplifiedGUIElement simpleGE) {

		// extract WebElement properties
		int wEX = wE.getX();
		int wEY = wE.getY();
		String wETag = wE.getTagName();

		// extract SimplifiedGUIElement properties
		
		int wGTX = simpleGE.getX();
		int wGTY = simpleGE.getY();
		String wGTTag = simpleGE.getTagName();

		boolean checkOne = wEX == wGTX;
		boolean checkTwo = wEY == wGTY;
		boolean checkThree = wETag.equals(wGTTag);

		return checkOne && checkTwo && checkThree;
	}

	/**
	 * Tests if the SeleneseWrapper and EFG event are matched (refer to the same
	 * thing).
	 * 
	 * Currently does the following checks: + Check widget ID
	 * 
	 * 
	 * @param seleneseStep
	 * @param event
	 * @return
	 */
	private boolean isMatched(SeleneseWrapper seleneseStep, EventType event) {

		String ssWidgetID = seleneseStep.getID();
		String evWidgetID = event.getWidgetId();

		boolean checkOne = ssWidgetID.equalsIgnoreCase(evWidgetID);

		return checkOne;
	}
	
	
	/**
	 * Added for convenience/efficiency. Stored path to EFG and GUI files
	 * 
	 * @author Thomas Irish
	 * 
	 */
	class ReferenceFileWrapper {

		String efg;
		String gui;
		
		AdditionalInfoFile info;
	}

	
}
