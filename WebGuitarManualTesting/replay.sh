#!/bin/sh

filename=$1
testcase=$2


if [ $filename = "-h" ]; then

	echo "Usage:"
	echo "bash $0 <filename> <testcase>"
	echo ""
	echo "	<filename> is the path to the EFG and GUI file (without the extensions)"
	echo "	<testcase> is the path to a .tst file>"
	echo ""
	exit

fi

#export GUITAR_OPTS="-Dlog4j.configuration=../log4j.properties"

bash ../dist/guitar/sel-replayer.sh -e $filename.EFG -g $filename.GUI -t $testcase -d 1000


