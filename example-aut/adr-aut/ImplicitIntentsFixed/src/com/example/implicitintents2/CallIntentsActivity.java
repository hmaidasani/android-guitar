package com.example.implicitintents2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class CallIntentsActivity extends Activity {
	private Spinner spinner;
	private Intent intent = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
//		spinner = (Spinner) findViewById(R.id.spinner);
//		ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
//				R.array.intents, android.R.layout.simple_spinner_item);
//		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		spinner.setAdapter(adapter);
		
		
	}


	public void onClickOpenBrowser(View view) {
		intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("http://www.vogella.de"));
		startActivity(intent);
	}
	public void onClickCallSomeone(View view) {
		intent = new Intent(Intent.ACTION_CALL,
				Uri.parse("tel:(+49)12345789"));
		startActivity(intent);
	}
	public void onClickDial(View view) {
		intent = new Intent(Intent.ACTION_DIAL,
				Uri.parse("tel:3014053333"));
		startActivity(intent);
	}
	public void onClickShowMap(View view) {
		intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("geo:38.986009,-76.942524?z=19"));
		startActivity(intent);
	}
	public void onClickSearchOnMap(View view) {
		intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("geo:38.986009,-76.942524?q=CSIC"));
		startActivity(intent);
	}
	public void onClickTakePicture(View view) {
		intent = new Intent("android.media.action.IMAGE_CAPTURE");
		startActivity(intent);
	}
	public void onClickShowContacts(View view) {
		intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("content://contacts/people/"));
		startActivity(intent);
	}

	
	/**
	 * <item>Open Browser</item>
        <item>Call Someone</item>
        <item>Dial</item>
        <item>Show Map</item>
        <item>Search on Map</item>
        <item>Take picture</item>
        <item>Show contacts</item>
        <item>Edit first contact</item>




         <Spinner
        android:id="@+id/spinner"
        android:layout_width="wrap_content"
        android:layout_height="135dp"
        android:layout_alignParentLeft="true"
        android:layout_centerVertical="true"
        android:drawSelectorOnTop="true" />

	 * @param view
	 */
//	public void onClick(View view) {
//		int position = spinner.getSelectedItemPosition();
//		Intent intent = null;
//		switch (position) {
//		case 0:
//			intent = new Intent(Intent.ACTION_VIEW,
//					Uri.parse("http://www.vogella.de"));
//			break;
//		case 1:
//			intent = new Intent(Intent.ACTION_CALL,
//					Uri.parse("tel:(+49)12345789"));
//			break;
//		case 2:
//			intent = new Intent(Intent.ACTION_DIAL,
//					Uri.parse("tel:(+49)12345789"));
//			startActivity(intent);
//			break;
//		case 3:
//			intent = new Intent(Intent.ACTION_VIEW,
//					Uri.parse("geo:50.123,7.1434?z=19"));
//			break;
//		case 4:
//			intent = new Intent(Intent.ACTION_VIEW,
//					Uri.parse("geo:0,0?q=query"));
//			break;
//		case 5:
//			intent = new Intent("android.media.action.IMAGE_CAPTURE");
//			break;
//		case 6:
//			intent = new Intent(Intent.ACTION_VIEW,
//					Uri.parse("content://contacts/people/"));
//			break;
//		case 7:
//			intent = new Intent(Intent.ACTION_EDIT,
//					Uri.parse("content://contacts/people/1"));
//			break;
//
//		}
//		if (intent != null) {
//			startActivity(intent);
//		}
//	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK && requestCode == 0) {
			String result = data.toURI();
			Toast.makeText(this, result, Toast.LENGTH_LONG);
		}
	}

}